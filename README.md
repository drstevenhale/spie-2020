# SPIE Astronomical Telescopes + Instrumentation 2020

[![pipeline status](https://gitlab.com/drstevenhale/spie-2020/badges/paper/pipeline.svg)](https://gitlab.com/drstevenhale/spie-2020/-/commits/paper)
[![SPIE](https://img.shields.io/badge/SPIE-doi%3A10.1117%2F12.2561282-fff100)](https://doi.org/10.1117/12.2561282)

A repository containing the source paper and poster for the
publication "[A next generation upgraded observing platform for the
automated Birmingham Solar Oscillations Network
(BiSON)](https://doi.org/10.1117/12.2561282)" published by
[International Society for Optics and
Photonics](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/11452.toc).

The final publications are available here: [`paper`](paper/1145222.pdf) [`poster`](poster/SPIE-AS20-9fc2f3ac-3205-ea11-813b-005056be78dc.pdf)

# Citation

```
@inproceedings{10.1117/12.2561282,
      author = {S. J. Hale and W. J. Chaplin and G. R. Davies and Y. P. Elsworth},
       title = {{A next generation upgraded observing platform for the automated Birmingham Solar Oscillations Network (BiSON)}},
      volume = {11452},
   booktitle = {Software and Cyberinfrastructure for Astronomy VI},
      editor = {Juan C. Guzman and Jorge Ibsen},
organization = {International Society for Optics and Photonics},
   publisher = {SPIE},
       pages = {363 -- 374},
    keywords = {robotic telescope, instrumentation, microelectromechanical systems (MEMS), Internet of Things (IoT), Industry 4.0 protocols, helioseismology, solar oscillations},
        year = {2020},
         doi = {10.1117/12.2561282},
         URL = {https://doi.org/10.1117/12.2561282}
}
```
