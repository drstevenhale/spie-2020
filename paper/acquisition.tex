% -*- coding: utf-8 -*-
%
% ACQUISITION.TEX
%
%   Steven Hale
%   2020 March 27
%   Birmingham, UK
%
% Astronomical Telescopes + Instrumentation 2020 in Yokohama, Japan
%

\section{SUPERVISORY CONTROL AND DATA ACQUISITION}
\label{sec:acquisition}

\begin{figure}[t]
  \begin{center}
    \begin{tabular}{c} %% tabular useful for creating an array of images 
      \includegraphics[width=0.7\textwidth]{acquisition}
      %\rule{.1pt}{5cm} \rule{10cm}{.1pt} \rule{.1pt}{5cm}
    \end{tabular}
  \end{center}
  \caption{\label{fig:microservices} A block diagram of the proposed
    microservice architecture for BiSON:NG, that is easily scalable to
    many small telescopes in a globally distributed network.}
\end{figure} 

In the early 1990s, the original network control system was based
around a standard desktop PC running Microsoft~DOS, and a Keithley
System~570 digital input/output interface for connection to dome,
telescope, and instrumentation control hardware.  The computer acted
as a centralised controller, handling all systems from dome and mount
pointing, to temperature stabilisation and data readout, using a
Supervisory Control and Data Acquisition (SCADA) monolithic control
system architecture.  The data would be retrieved each day over a
long-distance telephone call via dial-up modem, and later over the
internet.  The Keithley interface devices were used until 2002, when
the DOS PCs were replaced with the current systems running GNU/Linux
with hardware interconnects predominately via RS-232, although a PCI
general purpose digital-IO card is still used to interface to some
legacy equipment.

The so-called ``Industry~4.0'', the fourth industrial revolution, has
since brought about inter-connected machines, devices, and sensors --
the ``Internet of Things'' (IoT) -- that have allowed control systems
to become decentralised.  By migrating from a monolithic to a
microservice based architecture (MSA) in a Distributed Control System
(DCS) it is possible to further reduce both the physical footprint,
complexity, and deployment cost.  A microservice follows the Unix
philosophy of ``Do One Thing and Do It Well'', where each service is a
single purpose process capable of handling a request independently.
Such decoupled processes allow a modular approach to system design,
reducing development into more manageable components and removes the
need to understand the entire monolithic system.  Independent modules
also mean they can be independently upgraded or replaced, removing the
common risk of being forced to redesign an entire system simply
because one component has become obsolete.  When all devices and
sensors are connected to every other service, it can also become
possible trigger maintenance processes autonomously by monitoring data
from all points in the overall system and predicting potential
failures.  Such flexibility is essential in the operation of a
worldwide constellation of small telescopes, that inevitably become
heterogeneous due to gradual roll-out of repairs and upgrades.
Figure~\ref{fig:microservices} shows a block diagram of the proposed
MSA for BiSON:NG, and also for implementation at the existing network
telescopes.

Communication between microservices is typically handled using
lightweight protocols with well-defined inputs and outputs.  Most IoT
devices communicate over internet protocol networks making use of
industry standard Ethernet or WiFi.  This has a significant advantage
in allowing many devices to be easily connected using standard network
switches, rather than relying on the limited connectivity options
available on a single PC.  Components such as sensors and controllers
from different manufacturers need to communicate using standardised
communication protocols.  As is common with standards there are
several from which to choose, such as OPC~UA (Open Platform
Communications Unified Architecture), DDS (Data Distribution Service),
MQTT (Message Queuing Telemetry Transport), CoAP (Constrained
Application Protocol), Extensible Messaging and Presence Protocol
(XMPP), Advanced Message Queuing Protocol (AMQP), and
others\cite{8755050}.  Whilst there is no one best protocol, we have
chosen to use MQTT due to it being designed for low-bandwidth
connections from remote locations.  It is extremely lightweight, with
open source examples of implementation on micro-controllers where a
small code footprint is essential, and single-board computers, both of
which we use extensively.

MQTT is an ISO~standard client/server publish/subscribe protocol
developed by IBM\cite{mqtt}.  The protocol requires a message broker
through which clients communicate, with messages organised into
topics.  Messages are broadcast on a one-to-many distribution basis.
The protocol can scale easily up to hundreds or even thousands of
devices, allowing decoupling of applications since any service with an
interest in a particular data feed can simply subscribe to the
relevant topic.  Several quality-of-service (QoS) options are
available, ensuring messages are delivered either at most once, at
least once, or exactly once, providing robust communication even over
high-latency or unreliable networks.  Typically only a single broker
is required, although multiple brokers can be linked for either
redundancy, load sharing, or connecting between local and remote
sites.  There are also security advantages to using MQTT, since
communication can be easily authenticated over a Transport Layer
Security (TLS) encrypted channel.  The broker manages all security
credentials and certificates, and also tracks the client connection
states allowing rapid notification of a disconnected service.

A number of free and open-source packages are used where possible.
Data archival is handled both locally at each telescope, in order to
survive network outages, and remotely for aggregation.  A
timeseries-specific database is used, such as
Timescale\cite{timescale} and InfluxDB\cite{influxdb}, to ensure
performance at scale.  Platform monitoring and telemetry visualisation
is produced by Grafana\cite{grafana}, with system log messages written
locally and, where necessary, published to a Slack\cite{slack} channel
for urgent notification.  Public outreach is possible through
automatic status updates via social media such as
Twitter\cite{twitter}.

Through the upgrades discussed here, we move from a full rack of
electronics to physically small services closely coupled near their
respective areas of instrumentation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
