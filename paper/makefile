#
#  MAKEFILE
#
#    Steven Hale
#    2019 June 17
#    Birmingham, UK
#

################################################################

main = paper

svg := $(wildcard *.svg)
pdf := $(filter-out $(main).pdf, $(wildcard *.pdf))
png := $(wildcard *.png)
jpg := $(wildcard *.jpg)
tex := $(wildcard *.tex)

################################################################

.PHONY: all help clean spell count check view

all: $(main).pdf

help :
	@echo "Targets:"
	@echo "all        Make the PDF version of the report."
	@echo "clean      Clean up."
	@echo "spell      Spell-check the source file."
	@echo "count      Find the number of words in the document."
	@echo "check      Search the build log for various warnings."
	@echo "view       View the document."

clean :
	git clean -dxf

spell : $(main).ltx $(tex)
	$(foreach                        \
	   file,                         \
	   $^,                           \
	   aspell                        \
              --lang=en_GB               \
	      --personal=./aspell.en.pws \
	      --repl=./aspell.en.prepl   \
	      --mode=tex                 \
	      check $(file) ;            \
        )

count : $(main).ltx $(docs)
	texcount -inc -sum -total $(main).ltx 2> /dev/null

check: 	$(main).pdf
	-grep --color=always --ignore-case Warning $(main).log
	-grep --color=always --ignore-case erfull $(main).log

view : $(main).pdf
	okular $< &

################################################################

# Convert SVG to PDF

SVG2PDF = /usr/bin/inkscape --export-area-drawing

%.pdf : %.svg
	$(SVG2PDF) --file=$< --export-pdf=$@

################################################################

# Here is how to LaTeX the document

LATEX = pdflatex -halt-on-error -file-line-error

$(main).pdf : $(main).ltx        \
	      spie.cls           \
	      spiebib.bst        \
	      references.bib     \
	      $(svg:%.svg=%.pdf) \
	      $(pdf) \
	      $(png) \
	      $(jpg) \
	      $(tex)
	$(LATEX) $<
	bibtex $(main)
	$(LATEX) $<
	$(LATEX) $<
	$(LATEX) $<

################################################################

# Run in --silent mode unless the user sets VERBOSE=1 on the                    
# command-line.                                                                 

ifndef VERBOSE
.SILENT:
endif
