% -*- coding: utf-8 -*-
%
% MOUNT.TEX
%
%   Steven Hale
%   2020 March 27
%   Birmingham, UK
%
% Astronomical Telescopes + Instrumentation 2020 in Yokohama, Japan
%

\section{AUTOMATION OF A CONSUMER-GRADE TELESCOPE MOUNT}
\label{sec:mems}  % \label{} allows reference to this section

There are two key issues when attempting to automate a small
commercial~off-the-shelf (COTS) telescope mount.  The first is access
to a communications protocol with a published application programming
interface~(API) in order to allow full computer control of the mount.
Many COTS mounts do not have facility for computer control at all, and
rely only on the supplied proprietary control handset.  In cases where
the handset offers an ASCOM compatible control interface this
typically does not work without first calibrating the mount position
within the handset software, and they offer no facility to store a
permanent calibration since they are expected to be packed away at the
end of each observing session.  Typically movement is tracked by
counting stepper-motor steps once initial alignment has been
completed, and so the calibration is lost as soon as the mount is
moved by hand or the power is interrupted.  Larger, more expensive,
research-grade mounts employ absolute position encoders to avoid this
problem.  A fully automated telescope must be able to recover after a
power failure without manual recalibration.

The communication and control problem can be solved by simply choosing
only manufacturers that open-source and publish their control API.
The issue of alignment and recovery from power failure without user
intervention is rather more tricky.  Potentially a mount could be
retro-fitted with absolute position encoders, or limit switches to
indicate a home position, but this moves away from the intention of
simple off-the-shelf use.  Instead, it is possible to make use of
microelectromechanical systems (MEMS) sensors to directly detect the
attitude of the telescope.  Such devices are most commonly used in
smartphones and tablets, with accelerometers used to control screen
orientation, and magnetometers used to determine heading when
navigating.  These inexpensive sensors can be easily mounted on the
telescope itself and avoid costly modifications to the mount.

A consumer-grade equatorial mount was trialled at the Mount~Wilson
(Hale) Observatory 60~foot solar tower, an existing BiSON site, in
order to determine the possible accuracy of MEMS attitude sensors, and
the precision of solar autoguiding\cite{bison382}.
Figure~\ref{fig:enclosure}, left panel, shows the temporary binocular
configuration of fibre collection optics for solar data acquisition,
and a solar-filtered CCD camera for guiding.
Figure~\ref{fig:enclosure}, right panel, shows a prototype automated
weather-proof housing with a rolling-roof suitable for a small mount
and optics.  The final design and construction of an enclosure is
subject to further work.

In subsection~\ref{sec:mems-acc} below we demonstrate determining
telescope attitude from an ADXL345 three-axis accelerometer, in
subsection~\ref{sec:mems-mag} acquiring magnetic heading from a
HMC5883L three-axis magnetometer, and in subsection~\ref{sec:guiding}
guiding the mount on the centroid of the solar disc estimated using
computer vision techniques.

\begin{figure}[t]
  \begin{center}
    \begin{tabular}{c c} %% tabular useful for creating an array of images 
      \includegraphics[height=0.35\textwidth]{sunrise} &
      \includegraphics[height=0.35\textwidth]{enclosure}
    \end{tabular}
  \end{center}
  \caption{\label{fig:enclosure} Left: Sunrise at the Mount Wilson
    (Hale) Observatory\cite{epapers2987}. A small consumer-grade
    equatorial mount was configured with a binocular arrangement of a
    CCD camera for guiding, and optical fibre collection optics for
    data acquisition trial. Photo credit: S.\,J.\,Hale. Right: A small
    telescope enclosure and weather-proofing prototype with rolling
    roof.  Photo credit: Ash Manufacturing Company\cite{ashdome}.}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Determining attitude using a three-axis accelerometer}
\label{sec:mems-acc}

A MEMS accelerometer measures linear acceleration, and this includes
the acceleration due to the Earth's gravitational field vector.
Inside the device, a micro proof-mass is suspended by restoring
springs, and deflection of the proof-mass due to acceleration is
detected by measuring changes in capacitance between the proof-mass
and sensing plates.  An embedded micro-controller digitises the signal
and allows the data to be output over a digital serial bus.  A total
of three measurement axes are required in order to sense acceleration
in all directions.  A thorough discussion of calibration of a
three-axis accelerometer and extracting pitch and roll angles is given
in Ref.~\citenum{an3461} and~\citenum{an4399}.  We will now summarise
the calibration process and go on to apply the techniques to an
Analog~Devices~ADXL345 accelerometer fixed to the optical tube of an
equatorially mounted telescope.

The Analog~Devices~ADXL345 accelerometer is factory calibrated to
output values in units of $g$, with a precision of~\num{4}\,milli-$g$
per least-significant bit and a dynamic range of~\num{\pm16}\,$g$.
The accelerometer measures both the gravitational field vector, and
linear acceleration due to motion.  Measurement accuracy of the
gravitational field is reduced when the device is subject to
additional external accelerations, however on a quasi-static design
such as a telescope mount this is not a concern.  The factory
calibration is a 6-term model providing gain and offset parameters for
each of the three channels, and this generally provides adequate
results in the typical use case of orienting a smartphone.
Re-calibrating after the device is installed allows for greater
precision to be achieved by correcting for thermal stresses introduced
during soldering, and the convenience of allowing installation at an
arbitrary angle -- the device axes do not need to align with the
system axes.  A general 12-parameter calibrated output
$\mathbf{G}_{12}$ can be defined in terms of the factory calibration
$\mathbf{G}_{f}$ by~\cite[eq.~35]{an4399},
%
\begin{equation}\label{eq:G12_1}
    \mathbf{G}_{12}
     = \begin{pmatrix} G_{12x} \\ G_{12y} \\ G_{12z}\end{pmatrix}
     = \mathbf{W}\mathbf{G}_{f} + \mathbf{V}
     = \begin{pmatrix} W_{xx} & W_{xy} & W_{xz}\\
                       W_{yx} & W_{yy} & W_{yz}\\
                       W_{zx} & W_{zy} & W_{zz}\end{pmatrix}
       \begin{pmatrix} G_{fx} \\ G_{fy} \\ G_{fz}\end{pmatrix} +
       \begin{pmatrix} V_{x} \\ V_{y} \\ V_{z}\end{pmatrix} ~,
\end{equation}
%
where the gain matrix $\mathbf{W}$ includes any rotation of the
integrated circuit package and also corrects for all possible
cross-talk interactions, and $\mathbf{V}$ are the channel offsets.

If $M$ measurements are used for the 12-parameter calibration, then
the $i$-th measurement at pitch angle $\theta[i]$ and roll angle
$\phi[i]$ becomes~\cite[eq.~36]{an4399},
%
\begin{equation}%\label{eq:G12_2}
     \begin{pmatrix} G_{12x}[i] \\ G_{12y}[i] \\ G_{12z}[i]\end{pmatrix}
     = \begin{pmatrix} W_{xx} & W_{xy} & W_{xz}\\
                       W_{yx} & W_{yy} & W_{yz}\\
                       W_{zx} & W_{zy} & W_{zz}\end{pmatrix}
       \begin{pmatrix} G_{fx}[i] \\ G_{fy}[i] \\ G_{fz}[i]\end{pmatrix} +
       \begin{pmatrix} V_{x} \\ V_{y} \\ V_{z}\end{pmatrix}
     = \begin{pmatrix} -\sin(\theta[i]) \\ \cos(\theta[i]) \sin(\phi[i])\\ \cos(\theta[i]) \cos(\phi[i])\end{pmatrix} ~,
\end{equation}
%
which can be decomposed into three equations,
%
\begin{equation}%\label{eq:G12_3}
\begin{split}
   W_{xx} G_{fx}[i] + W_{xy} G_{fy}[i] + W_{xz} G_{fz}[i] + V_x &= -\sin(\theta[i]) \\
   W_{yx} G_{fx}[i] + W_{yy} G_{fy}[i] + W_{yz} G_{fz}[i] + V_y &=  \cos(\theta[i]) \sin(\phi[i]) \\
   W_{zx} G_{fx}[i] + W_{zy} G_{fy}[i] + W_{zz} G_{fz}[i] + V_z &=  \cos(\theta[i]) \cos(\phi[i]) ~,
\end{split}
\end{equation}
%
with residuals,
%
\begin{equation}\label{eq:G12_4}
\begin{split}
   r_x[i] &= -\sin(\theta[i]) - W_{xx} G_{fx}[i] - W_{xy} G_{fy}[i] - W_{xz} G_{fz}[i] - V_x \\
   r_y[i] &=  \cos(\theta[i]) \sin(\phi[i]) - W_{yx} G_{fx}[i] - W_{yy} G_{fy}[i] - W_{yz} G_{fz}[i] - V_y \\
   r_z[i] &=  \cos(\theta[i]) \cos(\phi[i]) - W_{zx} G_{fx}[i] - W_{zy} G_{fy}[i] - W_{zz} G_{fz}[i] - V_z ~.
\end{split}
\end{equation}
%

If we now consider only the $x$-component, equation~\ref{eq:G12_4} can
be simplified to~\cite[eq.~41]{an4399},
%
\begin{equation}%\label{eq:G12_5}
   r_x = Y_x - \mathbf{X}\beta_x ~,
\end{equation}
%
where $r_x$ is the $M$-length array of residuals to the calibration
fit, $Y_x$ is the array of $x$-components of the gravitation field for
the true measured angle, $X$ is the matrix of accelerometer
measurements, and finally $\beta_x$ is the solution vector for four of
the calibration parameters.  The optimum least squares solution for
$\beta_x$ can be found by making use of the Normal Equations for least
squares optimisation,
%
\begin{equation}\label{eq:leastsquares}
    \beta = (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \mathbf{Y} ~,
\end{equation}
%
such that~\cite[eq.~46]{an4399},
%
\begin{equation}%\label{eq:G12_6}
    \beta_x = \begin{pmatrix} W_{xx} \\ W_{xy} \\ W_{xz} \\ V_x \end{pmatrix}
            = (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T Y_x
            = (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T
              \begin{pmatrix} -\sin{\theta[0]} \\  -\sin{\theta[1]} \\ \dots \\ -\sin{\theta[M-1]} \end{pmatrix} ~,
\end{equation}
%
and similarly for the remaining two axes.  In order to solve for
12-parameters the minimum number of measurements $M$ is four, and
these should be well distributed in the measurement space.  More
measurements give a more robust calibration.  The calibrated
accelerations are subsequently determined using
equation~\ref{eq:G12_1}.

\begin{figure}[t]
  \begin{center}
    \begin{tabular}{c c} %% tabular useful for creating an array of images 
      \includegraphics[width=0.45\textwidth]{acc_raw} &
      \includegraphics[width=0.45\textwidth]{acc_calibrated}
    \end{tabular}
  \end{center}
  \caption{\label{fig:mems_acc}{Data from an Analog~Devices~ADXL345
      accelerometer fixed to the optical tube of an equatorially
      mounted telescope, slewed through its full range of motion.
      Left: The raw data with standard factory calibration.  Right:
      The data after 12-parameter calibration.  All points now sit on
      the surface of a sphere of radius $1\,g$.}}
\end{figure}

Figure~\ref{fig:mems_acc} shows a range of measurements made whilst
the mount was slewed slowly through its full range of motion.  Both
factory calibration and improved 12-parameter calibration are shown.
The roll and pitch angles can be computed from the calibrated
measurement matrix $\mathbf{G}_{12}$ using~\cite[eq.~25--26]{an3461},
%
\begin{equation}\label{eq:acc_roll1}
    \phi_{xyz} = \tan^{-1}\left(\frac{G_{12y}}{G_{12z}}\right) ~,
\end{equation}
%
and,
%
\begin{equation}%\label{eq:acc_pitch}
    \theta_{xyz} = \tan^{-1}\left(\frac{-G_{12x}}{\sqrt{{G_{12y}}^2 + {G_{12z}}^2}}\right) ~,
\end{equation}
%
where $\phi$ and $\theta$ are the measured roll and pitch angles
respectively, and the subscript $xyz$ notes that the angles are
according to the aerospace rotation sequence $\mathbf{R}_{xyz}$ where
rotation is first in yaw, then pitch, then roll.

Equation~\ref{eq:acc_roll1} becomes unstable when the telescope is
pointing near the zenith, since the $x$-axis becomes aligned with the
gravitational field vector and enters a condition known as Gimbal
Lock.  Any rotation in roll can no longer be detected and roll becomes
undefined.  A common work-around for this problem when using aerospace
rotation sequence $\mathbf{R}_{xyz}$ is to modify
equation~\ref{eq:acc_roll1} to include in the denominator a fraction
$\mu$ of the $x$-axis measurement whilst remembering to maintain the
sign of $G_{12z}$ after taking the square root~\cite[eq.~38]{an3461},
%
\begin{equation}%\label{eq:acc_roll2}
    \phi_{xyz} = \tan^{-1}\left(\frac{G_{12y}}{\pm \sqrt{{G_{12z}}^2 + \mu{G_{12x}}^2}}\right) ~,
\end{equation}
%
such that $\phi$ is slowly driven to zero as the telescope approaches
a vertical orientation.  An additional ambiguity is caused while the
telescope is vertical, since with an equatorial mount it becomes
impossible to determine if the telescope is on the east or west side
of the pier.  This ambiguity can be resolved by the addition of a
second accelerometer installed directly on the polar axis of the
mount.

In testing, the accelerometer achieved an accuracy of
approximately~\SI{\pm4}{\degree} in pitch and~\SI{\pm6}{\degree} in
roll.  The reduced performance in roll is due to the above work-around
at high pitch angles.  Performance from both axes is similar if only
moderate pitch angles are considered.  {M{\'e}sz{\'a}ros}
et.\,al.\,trialled the two-accelerometer technique by mounting two
Freescale MMA8453Q accelerometers on their telescope at Konkoly
Observatory located at the Piszk{\'e}stet{\"o} Mountain
station\cite{2014PASP..126..769M}, and achieved accuracy to better
than a degree with more sophisticated calibration, by making use of
full temperature compensation, and taking good care of power supply
stability via a custom printed circuit board.  The simpler treatment
shown here is adequate for a solar autoguider with a capture angle of
a few degrees.

Accelerometers are capable of completely resolving telescope attitude
only with an equatorial telescope mount.  With an altitude-azimuth
mount, only changes in altitude (pitch) can be measured.  Rotations in
azimuth (yaw) are aligned with the gravitational field vector and
cannot be detected, and so a different sensor is required.  Next, we
look at using a three-axis magnetometer to detect the orientation of
Earth's magnetic field and directly measure the telescope ``heading''
angle.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Determining heading using a three-axis magnetometer}
\label{sec:mems-mag}

In order to detect rotation about a vector parallel to Earth's
gravitational field vector (i.e., yaw, heading, azimuth) we need to be
able to detect rotation within Earth's magnetic field, otherwise known
as an electronic compass.  We used a Honeywell HMC5883L three-axis
magnetometer, which uses magneto-resistive sensors to measure both the
direction and the magnitude of Earth's magnetic field.  The Honeywell
HMC5883L magnetometer is factory calibrated to output values in units
of~\si{\micro\tesla}, with a dynamic range of~\SI{800}{\micro\tesla},
and an embedded 12-bit ADC providing a
documented~\SIrange{1}{2}{\degree} heading accuracy.  As with the
accelerometer, a magnetometer requires a final in-situ calibration in
order to achieve the best precision.  A thorough discussion of
calibration of a three-axis magnetometer is given in
Refs.~\citenum{an4246, an4247, an4248, an4249}.  We will now summarise
the calibration process and go on to apply the techniques to the
Honeywell HMC5883L, including tilt-compensation based on earlier
results from the Analog~Devices~ADXL345 accelerometer.

A magnetic vector measured at the device $\mathbf{B}_{d}$ can be
defined after arbitrary device rotation in terms of the local
geomagnetic field vector $\mathbf{B}$ by~\cite[eq.~5]{an4246},
%
\begin{equation}\label{eq:B4_1}
    \mathbf{B}_{d}
     = \mathbf{W} \mathbf{R}_x(\phi) \mathbf{R}_y(\theta) \mathbf{R}_z(\psi)
     \lVert \mathbf{B} \rVert \begin{pmatrix} \cos{\delta} \\ 0 \\ \sin{\delta}\end{pmatrix}
     + \mathbf{V} ~,
\end{equation}
%
where $\phi$, $\theta$, and $\psi$ are roll, pitch, and yaw angles as
previously, $\delta$ is the magnetic inclination at the measurement
location, $\mathbf{V}$ is the ``hard-iron'' offset vector, and
$\mathbf{W}$ is the ``soft-iron'' gain matrix.  So-called hard-iron
offsets are magnetic fields generated by nearby permanent magnets,
such as other components on the PCB, and motors in the telescope
mount.  These components are generally in fixed positions and rotate
with the device, and so they appear as an additive magnetic field
vector within the reference frame of the magnetometer.  So-called
soft-iron interference is due to temporary induction of magnetic
fields in otherwise normally unmagnetised components, such as the
sheet steel of the telescope mount and housing, caused by the
geomagnetic field itself.  Soft-iron effects are much more complicated
to model since they depend on the orientation of the device within the
geomagnetic field, and typically suffer magnetic hysteresis effects as
the device rotates.  The soft-iron matrix $\mathbf{W}$ is a 9-element
matrix similar to that used during calibration of the accelerometer in
equation~\ref{eq:G12_1}, and in addition to calibrating soft-iron
effects also calibrates rotation of the integrated circuit package and
corrects for all possible cross-talk interactions and gain variations
between channels.  In addition to removing offsets, the measurements
from a magnetometer need to be derotated back to the flat plane where
$\phi = \theta = 0$, since as with a typical analogue compass it works
only when held level.  Rearranging equation~\ref{eq:B4_1} for $\psi$
we get~\cite[eq.~6]{an4246},
%
\begin{equation}%\label{eq:B4_2}
\begin{split}
   \mathbf{R}_z(\psi) \lVert \mathbf{B} \rVert \begin{pmatrix} \cos{\delta} \\ 0 \\ \sin{\delta}\end{pmatrix}
   &= \begin{pmatrix}  \cos{\psi} & \sin{\psi} & 0 \\
                      -\sin{\psi} & \cos(\psi) & 0 \\
                                0 &          0 & 1 \end{pmatrix}
     \lVert \mathbf{B} \rVert \begin{pmatrix} \cos{\delta} \\ 0 \\ \sin{\delta}\end{pmatrix} \\
   &= \mathbf{R}_y(-\theta) \mathbf{R}_x(-\phi) \mathbf{W}^{-1}
     (\mathbf{B}_{d} - \mathbf{V}) ~,
\end{split}
\end{equation}
%
and so~\cite[eq.~9]{an4246},
%
\begin{equation}%\label{eq:B4_3}
   \begin{pmatrix} B_{fx} \\ B_{fy} \\ B_{fz}\end{pmatrix}
   = \begin{pmatrix}  \cos{\psi} \cos{\delta} \lVert \mathbf{B} \rVert \\
                     -\sin{\psi} \cos{\delta} \lVert \mathbf{B} \rVert \\
                                 \sin{\delta} \lVert \mathbf{B} \rVert \end{pmatrix}
   = \mathbf{R}_y(-\theta) \mathbf{R}_x(-\phi) \mathbf{W}^{-1}
     (\mathbf{B}_{d} - \mathbf{V}) ~,
\end{equation}
%
where $\mathbf{B}_{f}$ is equal to the magnetometer measurements with
both soft-iron and hard-iron effects removed, and derotated to a flat
plane where the $z$-component $B_{fz}$ is equal to $\lVert \mathbf{B}
\rVert \sin{\delta}$.  The yaw angle, or compass heading, is found
from~\cite[eq.~10]{an4246},
%
\begin{equation}\label{eq:mag_yaw}
    \psi = \tan^{-1}\left(\frac{-B_{fy}}{B_{fx}}\right) ~,
\end{equation}
%
and this simply requires the addition of the known local magnetic
declination to convert from magnetic north to true north, and so
obtain the azimuthal angle.

In many cases the soft-iron effects are insignificant, and only the
hard-iron offsets dominate.  This allows a simplification during
calibration since only four parameters need to be determined, and
these are the magnitude of the geomagnetic field strength
$\lVert \mathbf{B} \rVert$, and the three components of the hard-iron
vector $\mathbf{V}$.  The soft-iron matrix $\mathbf{W}$ becomes the
identity matrix.  With these assumptions we can follow the same
calibration procedure as for the accelerometer, by developing a
performance function to be minimised by optimising the calibration fit
and again using equation~\ref{eq:leastsquares} to solve the fit
through matrix algebra.

\begin{figure}[ht]
  \begin{center}
    \begin{tabular}{c c} %% tabular useful for creating an array of images 
      \includegraphics[width=0.45\textwidth]{mag_raw} &
      \includegraphics[width=0.45\textwidth]{mag_calibrated}
    \end{tabular}
  \end{center}
  \caption{\label{fig:mems_mag} {Data from a Honeywell HMC5883L
      magnetometer slewed randomly through the full range of motion.
      Left: The raw data with a hard-iron offset. Right: The blue dots
      show the data after 4-parameter calibration removing the
      hard-iron offset.  All points now sit on the surface of a sphere
      centred at the origin, with radius equal to the local
      geomagnetic field strength in Birmingham calibrated at
      \SI{54.5}{\micro\tesla}.  The green dots show the same data
      after derotation to the flat plane where $\phi = \theta = 0$ and
      the $z$-component $B_{fz}$ is equal to $\lVert \mathbf{B} \rVert
      \sin{\delta}$.}}
\end{figure}

Figure~\ref{fig:mems_mag} shows a range of measurements made by a
Honeywell HMC5883L magnetometer slewed randomly through the full range
of motion.  It is clear from the left panel of
Figure~\ref{fig:mems_mag} that the data do indeed suffer from a
hard-iron offset, which moves the data away from the origin.  After
applying the 4-parameter calibration, shown by the blue dots in the
right panel of Figure~\ref{fig:mems_mag}, the data now sit on the
surface of a sphere centred at the origin, with radius equal to the
local geomagnetic field strength in Birmingham calibrated
at~\SI{54.5}{\micro\tesla}.  Our assumption that the soft-iron effects
are insignificant is shown to be true, since these would have the
effect of distorting the data away from a perfect sphere and into an
ellipse.  The green dots show the same data after derotation, by
making use of the pitch and roll angles determined by the
accelerometer, to the flat plane where $\phi = \theta = 0$ and the
$z$-component $B_{fz}$ is equal to $\lVert \mathbf{B} \rVert
\sin{\delta}$.  These data are now calibrated and ready for the
heading to be extracted using equation~\ref{eq:mag_yaw}.  The
resulting accuracy after calibration is
approximately~\SI{\pm5}{\degree}.  With further work and more careful
calibration, it is expected to achieve the manufacturers stated
performance of~\SIrange{1}{2}{\degree} resolution.  With this simple
calibration, and as with the accelerometer, the performance is
adequate for a solar autoguider with a capture angle of a few degrees.

Having shown that it is possible to obtain sufficiently accurate
absolute pointing information of an inexpensive telescope mount at
power-on, we will now go on to consider fine guiding of a mount once
the target has been acquired by the coarse pointing.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Solar auto-guiding using computer vision}
\label{sec:guiding}

After the Sun is brought within the capture angle of the autoguider
camera by the coarse MEMS-based pointing, mount control is handled
purely via imaged-based guiding much like any other telescope.  The
camera trialled has a~\SI{6.4}{\milli\metre}
by~\SI{4.75}{\milli\metre} CCD with~\SI{4.65}{\micro\metre} square
pixels in a~\num{1392} by \num{1040}~array.  When coupled with
an~\SI{80}{\milli\metre} focal length objective lens this produces an
approximate field of view of~\SI{4.6}{\degree} by~\SI{3.5}{\degree},
where each pixel has about~\SI{12}{\arcsecond} field of view.  The Sun
has an extent of about~\SI{32}{\arcminute} and so produces an image
about 160~pixels in diameter on the sensor.  Two filters were used to
bring the image within the dynamic range of the CCD chip.  These were
a neutral density filter with optical density of~\num{5} (i.e,
transmission of approximately $10^{-5}$), and a~\SI{10}{\nano\metre}
bandpass filter centered on~\SI{780}{\nano\metre}. Images were read
from the camera as frequently as possible, approximately once per
second.  The image exposure time was~\SI{79}{\milli\second}, but the
cadence was restricted by the CCD read-out time and USB transfer rate.

Images from the camera were processed to determine the position of the
Sun to sub-pixel accuracy using the OpenCV (Open-source
Computer-Vision) library\cite{opencv_library}, running on an
inexpensive single-board-computer. The centroid position of the solar
disc was found using by applying a black and white threshold, finding
the contours in the image, and subsequently reading out the contour
centroid position.  The guiding error was determined by comparing the
current solar centroid position with a desired target value.  The
position error for each axis was then passed through a
proportional–integral–derivative (PID) control loop feedback mechanism
in order to determine the correct mount drive rate, and the mount
motors updated with the new drive rate using the motor API published
by the mount manufacturer.  The PID control algorithm is a servo
feedback system that can be defined simply by,
%
\begin{equation}
  \mathrm{Output} = K_P \, e(t) +
                    K_I \int{e(t)\,dt} +
                    K_D \frac{\mathrm{d}e(t)}{\mathrm{d}{t}} ~,
\end{equation}
%
where $e$ is the control error defined as the desired setpoint minus
the current value, and $K_P$, $K_I$, and $K_D$ are the proportional,
integral, and derivative coefficients.  The solar guiding algorithm
made use of only the proportional and integral parameters.

\begin{figure}[t]
  \begin{center}
    \begin{tabular}{c} %% tabular useful for creating an array of images 
      \includegraphics[scale=1.0]{mo-guider-160925-hist}
    \end{tabular}
  \end{center}
  \caption{\label{fig:mo160925_hist} Guider performance histogram.
    The dashed red line indicates the equivalent Gaussian for the
    measured median and standard deviation.}
\end{figure} 

The distribution of position-error for both axes, calibrated in
arcseconds, is shown in Figure~\ref{fig:mo160925_hist}.  The control
algorithm achieves performance on both axes to better
than~\SI{\pm8}{\arcsecond} when logged at an
approximate~\SI{1}{\hertz} cadence over an entire day.  This is more
than sufficient for Sun-as-a-star observations when considering the
Sun is over~\SI{1800}{\arcsecond} in observed diameter.

By making use of these techniques is it possible to achieve full
automation of consumer-grade telescope mounts.  In the next section we
will discuss prototype updates to the BiSON control systems.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
